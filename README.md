# Zabbix_Ceph_Softiron

A Zabbix template to monitor Ceph from a Softiron storage system.

I'm not affiliated in anyway to either Zabbix or Softiron. Just a user of both.

This template is a work in progress as I build it out. Feedback and contributions are welcome.

Since Zabbix itself is GPLv2 I figure that is good enough for this too.